import TableCell from '@material-ui/core/TableCell';
import { Input } from '@material-ui/core';
import React from 'react';

interface ICustomTableCell {
    row: any;
    name: string;
    onChange: any;
}

const CustomTableCell: React.FC<ICustomTableCell> = ({ row, name, onChange }) => {
  const { isEditMode } = row;
  return (
    <TableCell>
      {isEditMode ? (
        <Input
          value={row[name]}
          name={name}
          onChange={e => onChange(e, row)}
        />
      ) : (
        row[name]
      )}
    </TableCell>
  );
};

export default CustomTableCell;
