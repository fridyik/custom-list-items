import React from 'react';

import devices from '../../data/device.json';

interface IDevicesProps {
    projectId: number;
}

const Devices: React.FC<IDevicesProps> = ({ projectId }) => (
    <>
        {devices
            .filter((device) => device.projectId === projectId)
            .map((device) => device.serialNumber,
            )}
    </>
);

export default Devices;
