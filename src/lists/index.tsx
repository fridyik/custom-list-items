import React from 'react';
import Devices from './devices';
import project from '../data/project.json';
import Users from './users';
import TableHead from '@material-ui/core/TableHead';
import { Button, TableCell } from '@material-ui/core';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper';
import TableBody from '@material-ui/core/TableBody';
import { Delete } from '@material-ui/icons';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});


const rows = project.map((item) => (
    {
        id: item.id,
        project: item.title,
        device: <Devices projectId={item.id}/>,
        users: <Users projectId={item.id}/>,
        beginDate: new Date(item.beginDate).toLocaleDateString(),
        expireDate: item.expirationDate ? new Date(item.expirationDate).toLocaleDateString() : 'No expire date',
    }
));

export default function DenseTable() {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow key={'HeadRow'}>
                        <TableCell>Project</TableCell>
                        <TableCell>Devices</TableCell>
                        <TableCell>Users</TableCell>
                        <TableCell>Start Date</TableCell>
                        <TableCell>Expire Date</TableCell>
                        <TableCell>Actions</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell>{row.project}</TableCell>
                            <TableCell>{row.device}</TableCell>
                            <TableCell>{row.users}</TableCell>
                            <TableCell>{row.beginDate}</TableCell>
                            <TableCell>{row.expireDate}</TableCell>
                            <TableCell>
                                <Button onClick={() => alert('Item should be deleted')}>
                                    <Delete/>
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

