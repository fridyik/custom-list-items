import React from 'react';

import users from '../../data/user.json';

interface IDevicesProps {
    projectId: number;
}

const Users: React.FC<IDevicesProps> = ({ projectId }) => (
    <>
        {users
            .filter((user) => user.projectId === projectId)
            .map((user, index) => (
                    <div key={index}>{user.firstName} {user.lastName}</div>
                ),
            )
        }
    </>
);

export default Users;
